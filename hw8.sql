--1--
SELECT 
    to_char(sum(salary), 'L999G999G999D99') AS salary
FROM
    hr.employees
GROUP BY
    department_id
;

--2--
SELECT 
    trunc(hire_date, 'yyyy') AS hire_year
FROM
    hr.employees
;

--3--
SELECT 
    to_char(trunc(hire_date, 'yyyy'), 'yyyy') AS char_year
FROM
    hr.employees
;

--4--
SELECT 
    to_number(to_char(trunc(hire_date, 'yyyy'), 'yyyy')) AS num_year
FROM
    hr.employees
;

--5--
SELECT 
    min(to_number(to_char(trunc(hire_date, 'yyyy'), 'yyyy'))) AS min_year
    , round(avg(to_number(to_char(trunc(hire_date, 'yyyy'), 'yyyy'))), 5) AS avg_year
    , max(to_number(to_char(trunc(hire_date, 'yyyy'), 'yyyy'))) AS max_year
FROM
    hr.employees
;

--6--
CREATE TABLE hurricane (
                        name        varchar2(64),
                        report_year date,
                        victims     number
                        )
;

--7--
ALTER TABLE hurricane MODIFY name NOT NULL
;

--8--
TRUNCATE TABLE hurricane
;

--9--
DROP TABLE hurricane
;
